# variables.tf contains definitions of variables used by the module.

variable "project" {
  description = <<EOF
    Project for cluster. Cannot be inferred from Google provider since there is no
    way to interpolate provider-level configuration.
EOF

}

variable "location" {
  description = <<EOF
    Region for cluster. Cannot be inferred from Google provider since there is no way to interpolate
    provider-level configuration.
EOF

}

variable "name" {
  default     = "cluster"
  description = "Name of the cluster"
}

variable "service_account" {
  description = <<EOI
A custom service account for the cluster nodes to run as.
It is strongly recommended to set this and not to use the default service account.
EOI
  type        = string
  default     = ""
}

variable "cluster_secondary_range_name" {
  default = ""

  description = <<EOF
    This variable is required if subnetwork is set. Specifies the name of the secondary range
    in the subnetwork that is going to be used for the pod IP addresses.
EOF

}

variable "services_secondary_range_name" {
  default = ""

  description = <<EOF
    This variable is required if subnetwork is set. Specifies the name of the secondary range
    in the subnetwork that is going to be used for the services in the cluster.
EOF

}

variable "cluster_ipv4_cidr_block" {
  default     = ""
  description = "The IP address range for the cluster pod IPs"
}

variable "services_ipv4_cidr_block" {
  default     = ""
  description = "The IP address range of the services IPs in this cluster"
}

variable "machine_type" {
  description = "Default machine type"
  default     = "e2-standard-2"
}

variable "disk_size_gb" {
  default     = 50
  description = "Size of the disk attached to each node, specified in GB."
}

variable "disk_type" {
  default     = "pd-ssd"
  description = "Type of the disk attached to each node (e.g. 'pd-standard' or 'pd-ssd')"
}

variable "node_count" {
  default     = 1
  description = "Number of nodes per zone"
}

variable "autoscaling" {
  default     = null
  description = <<EOI
Minimum and maximum nodes per zone when autoscaling. For example: {min_node_count = 1, max_node_count = 3}
EOI

  type = object({
    min_node_count = number
    max_node_count = number
  })
}

variable "autoscaling_profile" {
  default     = "BALANCED"
  description = <<EOI
Configuration options for the Autoscaling profile feature, which lets you choose whether the cluster autoscaler
should optimize for resource utilization or resource availability when deciding to remove nodes from a cluster.
Can be BALANCED or OPTIMIZE_UTILIZATION. Defaults to BALANCED.
EOI

  type = string
}


# Cluster tags, which allow you to make firewall rules/routes applicable to specific VM instances/nodes.
variable "cluster_tags" {
  default     = []
  description = "Names of the cluster tags"
}

# IP masquerade agent
variable "enable_ip_masq" {
  description = "Enables IP masquerade agent. It is not required when using aliasied IPs."
  default     = false
}

variable "non_masquerade_cidrs" {
  type        = list(string)
  description = "IP address ranges that do not use IP masquerading."
  # these are default values for GKE versions 1.14.1-gke.14, 1.14.2-gke.1 and later.
  default = [
    "10.0.0.0/8",
    "172.16.0.0/12",
    "192.168.0.0/16",
    "100.64.0.0/10",
    "192.0.0.0/24",
    "192.0.2.0/24",
    "192.88.99.0/24",
    "198.18.0.0/15",
    "198.51.100.0/24",
    "203.0.113.0/24",
    "240.0.0.0/4"
  ]
}

variable "ip_masq_resync_interval" {
  type        = string
  description = "The interval at which the agent attempts to sync its ConfigMap file."
  default     = "60s"
}

variable "ip_masq_link_local" {
  type        = bool
  description = "Masquerade traffic to the link-local prefix (169.254.0.0/16)."
  default     = false
}

variable "enable_workload_identity" {
  default     = true
  type        = bool
  description = "If true, Workload Identity is enabled in the cluster. Defaults to true."
}

variable "enable_secret_manager" {
  default     = false
  type        = bool
  description = <<EOF
    If true, Secret Manager add-on is enabled in the cluster. Defaults to false.
    See https://cloud.google.com/secret-manager/docs/secret-manager-managed-csi-component
  EOF
}

variable "istio_disabled" {
  default     = true
  description = "Set to false to enable istio for the cluster."
}

variable "istio_auth" {
  default     = "AUTH_MUTUAL_TLS"
  description = "Mutual authentication mode for istio."
}

variable "enable_secure_boot" {
  default     = true
  description = "Enable secure boot for cluster nodes"
}

variable "enable_integrity_monitoring" {
  default     = true
  description = "Enable integrity monitoring for cluster nodes"
}

variable "enable_gvisor_sandbox" {
  default     = false
  description = <<-EOF
    Enable use of gvisor sandbox. This requires changes to the pod configuration
    as documented at
    https://cloud.google.com/kubernetes-engine/docs/how-to/sandbox-pods.
  EOF
}

variable "release_channel" {
  default = "REGULAR"
  validation {
    condition     = contains(["UNSPECIFIED", "RAPID", "REGULAR", "SECURE", "STABLE"], var.release_channel)
    error_message = "Has to be one of UNSPECIFIED, RAPID, REGULAR, SECURE or STABLE."
  }

  description = <<-EOF
    Release channel for cluster. Defaults to "REGULAR". See
    https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/container_cluster#release_channel
    for all options.
  EOF
}

variable "min_master_version" {
  description = <<EOT
The minimum version of the master.
GKE will auto-update the master to new versions, so this does not guarantee the current master version - use the read-only
master_version field to obtain that. If set to the default empty string, the cluster's version will be set by GKE to the
version of the most recent official release (which is not necessarily the latest version). Most users will find the
google_container_engine_versions data source useful - it indicates which versions are available, and can be use to approximate
fuzzy versions in a Terraform-compatible way. If you intend to specify versions manually,
the docs (https://cloud.google.com/kubernetes-engine/versioning#specifying_cluster_version) describe the various acceptable formats
for this field.
EOT
  type        = string
  default     = null
}

variable "enable_autopilot" {
  default     = false
  description = <<-EOF
    Enable Autopilot for this cluster. Defaults to false. Note that when this option is 
    enabled, certain features of Standard GKE are not available. 
    See the official documentation for available features.
    https://cloud.google.com/kubernetes-engine/docs/concepts/autopilot-overview#comparison
  EOF
}

variable "alerting_enabled" {
  type        = bool
  default     = true
  description = "Enable alerting metrics and policies."
}

variable "alerting_notification_channel_ids" {
  default     = []
  type        = list(string)
  description = <<EOT
    Notification channel ids that any alerts which are generated should be sent to.
    If not specified, alerts will still be generated (if they are enabled), but no
    notifications will be sent.
EOT
}

variable "cluster_notification_enabled" {
  type        = bool
  default     = false
  description = <<EOT
    Whether cluster notifications are enabled. When enabled a topic must be provided
    in the cluster_notification_topic_id variable.
EOT
}

variable "cluster_notification_topic_id" {
  type        = string
  default     = null
  description = <<EOT
    Topic id which GKE can publish information about events relevant to your cluster configuration, 
    such as available upgrades and security bulletin.
    Topic id is in the format projects/{{project}}/topics/{{name}}

    See the official documentation:
    https://cloud.google.com/kubernetes-engine/docs/concepts/cluster-notifications
EOT
}

variable "autopilot_cloud_dns_enabled" {
  type        = bool
  default     = true
  description = <<EOT
    Whether CLOUD_DNS is enabled for Autopilot cluster. For existing Autopilot clusters which
    haven't been migrated to CLOUD_DNS and remain on kube-dns, this should be set to false. Any
    new or migrated Autopilot clusters should have this set to true.
EOT
}

variable "networking_mode" {
  type        = string
  default     = "VPC_NATIVE"
  description = <<EOT
    Determines whether alias IPs or routes will be used for pod IPs in the cluster. Options are VPC_NATIVE or ROUTES.
    VPC_NATIVE enables IP aliasing. Newly created clusters should use VPC_NATIVE by default.
    In order to preserve old behaviour for previously created clusters, set this variable to "ROUTES" explicitly.
EOT
}

variable "resource_labels" {
  type        = map(string)
  default     = null
  description = "The GCE resource labels (a map of key/value pairs) to be applied to the cluster."
}

variable "enable_network_policy" {
  type        = bool
  default     = false
  description = "NetworkPolicy feature for GKE cluster."
}

variable "maintenance_policy" {
  description = <<EOT
    The maintenance policy to use for the cluster.
    By default, the cluster configured to have daily maintenance window from 1:00 a.m. to 5:00 a.m.
    Parameters 'start_time' and 'end_time' must be provided in RFC3339 'Zulu' date format. 
    Parameter 'recurrence' must be provided in RFC5545 RRULE format.
EOT
  type = object({
    recurring_window = optional(object({
      start_time = string
      end_time   = string
      recurrence = string
    }))
  })
  default = {
    recurring_window = {
      start_time = "2000-01-01T01:00:00Z"
      end_time   = "2000-01-01T05:00:00Z"
      recurrence = "FREQ=DAILY"
    }
  }
}

variable "deletion_protection" {
  description = "Whether the cluster is protected against deletion."
  type        = bool
  default     = false
}
