# Both the google and google-beta provider are configured identically to use
# default application credentials.
provider "google" {
  version = "~> 3.54"
}

# Kubernetes provider

# The google_client_config data source fetches a token from the Google Authorization
# server, which expires in 1 hour by default.
data "google_client_config" "default" {
}
provider "kubernetes" {
  host                   = "https://${module.cluster.endpoint}"
  token                  = data.google_client_config.default.access_token
  cluster_ca_certificate = base64decode(module.cluster.master_auth_cluster_ca_certificate)
}

provider "helm" {
  kubernetes {
    host                   = "https://${module.cluster.endpoint}"
    token                  = data.google_client_config.default.access_token
    cluster_ca_certificate = base64decode(module.cluster.master_auth_cluster_ca_certificate)
  }
}
