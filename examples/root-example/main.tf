# A Kubernetes cluster.
module "cluster" {
  source = "git::ssh://git@gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gke-cluster.git"

  project  = var.project
  location = "europe-west2"

  machine_type = "n1-standard-1"
}
