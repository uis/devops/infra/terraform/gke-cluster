# Google Kubernetes Engine cluster

This module contains configuration to deploy a Kubernetes cluster within a
Google Cloud Platform project. The cluster has regional availability but with a
configurable machine type.

This module conforms to the [terraform standard module
structure](https://www.terraform.io/docs/modules/create.html#standard-module-structure).

The email and credentials for the service account are available from the project
[outputs](outputs.tf). Configuration variables are documented in [variables.tf](variables.tf).

Please make sure that there are no conflicts when setting secondary range names 
(`cluster_secondary_range_name` and `services_secondary_range_name`) and IPv4 CIDR blocks 
(`cluster_ipv4_cidr_block` and `services_ipv4_cidr_block`).

## Versioning

The `master` branch contains the tip of development and corresponds to the `v4`
branch. The `v1` branch will maintain source compatibility with the initial
release.

## Google provider v4 AutoPilot issues

There is currently a bug in the google provider version `> 4.3.0` affecting GKE autopilot clusters.
See https://github.com/hashicorp/terraform-provider-google/issues/10782 for more info. To work
around this you should restrict the `google` and `google-beta` provider versions to `~> 4.3.0` in your `versions.tf`.
The version is not restricted in this module as the issue does not affect standard GKE cluster deployments,
which should continue to use the latest version of google provider v4 where possible.

## Service account for GKE AutoPilot cluster

Starting from version 6.0.0 of this module, if the `service_account` parameter is set in combination with
`enable_autopilot = true`, the service account will be used for GKE's nodes instead of the "default" one.
Please note, that it only works for new clusters. It is impossible to add or change
the `service_account` value for the existing cluster.

## Deprecations

Google's upstream GKE module recommends that one uses `..._secondary_range_name`
rather than `..._ipv4_cidr_block` to specify IP allocation policies. To preserve
backward compatibility, `..._ipv4_cidr_block` ranges are still supported but new
deployments should use `..._secondary_range_name`.

## Cluster hardening

In accordance with [Google's hardening
guidelines](https://cloud.google.com/kubernetes-engine/docs/how-to/hardening-your-cluster):

* Secure boot and integrity monitoring for all cluster nodes is enabled by
    default. (You may wish to disable these if you have need to install custom
    kernel modules.)
* Istio is disabled by default as it is currently in beta. When enabled it is
    configured to automatically perform mutual TLS auth between containers. This
    should be mostly transparent. **Enabling istio can significantly increase
    the resource usage of the cluster.**
* Workload identity is enabled by default. Along with providing welcome
    integration with Cloud IAM, This is important to prevent a class
    of node metadata exfiltration attacks.
* [gvisor](https://github.com/google/gvisor) is supported for container
    isolation although is is **disabled** by default since it requires pods be
    [explicitly marked as supporting
    isolation](https://cloud.google.com/kubernetes-engine/docs/how-to/sandbox-pods).

The following hardening guidelines are not followed for technical reasons:

* A public k8s API endpoint is maintained. Our current terraform configurations
    use the k8s provider and that provider talks directly to the endpoint. It
    would require some networking tricks on each machine running terraform for
    this to be done via a private IP.
* Private node IPs are not configured. This is mostly due to there not being a
    "sane default" for the
    [master_ipv4_cidr_block](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/container_cluster#master_ipv4_cidr_block)
    attribute since it requires manual allocation of IP space. If we develop a
    private IP space allocation scheme in future, we can expose this as an
    optional setting.
* Google group-based authentication is not enabled as we do not directly perform
    API-based cluster management as ordinary users.

The following hardening techniques are not implemented in the module as they are
best configured in each deployment:

* Using custom ClusterRoleBindings to [restrict cluster API
    discovery](https://cloud.google.com/kubernetes-engine/docs/how-to/hardening-your-cluster#restrict_access_to_cluster_api_discovery).
    This module does not create any k8s service account identities beyond the
    master admin identity and so other role bindings are out-of-scope.
* Using [Pod security
    policies](https://cloud.google.com/kubernetes-engine/docs/how-to/hardening-your-cluster#admission_controllers)
    to lock down per-pod permissions. Since pods are configured outside of this
    module, an appropriate pod security model is out-of-scope.
* Selecting an appropriate [release
    channel](https://cloud.google.com/kubernetes-engine/docs/concepts/release-channels)
    for the cluster. This module defaults to the "Regular" release channel which
    should be suitable for common cases. Especially sensitive deployments may
    want to set this to "STABLE" and development deployments may want to set
    this to "RAPID".

## Secret Store CSI Driver

The Secrets Store CSI driver for Kubernetes secrets ntegrates secrets stores with Kubernetes via 
a Container Storage Interface (CSI) volume. The Secrets Store CSI driver secrets-store.csi.k8s.io 
allows Kubernetes to mount multiple secrets, keys, and certs stored in enterprise-grade external 
secrets stores into their pods as a volume. Once the Volume is attached, the data in it is mounted 
into the container's file system.

This relies on a Helm chart that is currently only available in [GitHub](https://github.com/kubernetes-sigs/secrets-store-csi-driver)
Thus, we can't pin a version and upgrades to this will be executed when available.

We also install the Google Secret Manager Provider for Secret Store CSI Driver.
This is to be able to access [Google Cloud Secret Manager secrets](https://github.com/GoogleCloudPlatform/secrets-store-csi-driver-provider-gcp)

This is in Beta and to upgrade this driver we need to keep an eye on changes that happen
in their [GitHub repository](https://github.com/GoogleCloudPlatform/secrets-store-csi-driver-provider-gcp/blob/main/deploy/provider-gcp-plugin.yaml).

Changes ocurring in that reposity will have to be applied to the `secrets_store_csi_driver.tf` file.

## Monitoring - Metrics and Alerting

Logs based metrics are gathered for:
- `no scale down of a node` by the cluster autoscaler (CA), which can lead to additional running costs:
  - purpose: when the CA is unable to scale down a node
  - name: `gke-cluster/node-no-scale-down-event`
  - alert generated: `Yes`

Alerts are generated for any metrics which need to be brought to the attention of
product owners or admins. By default, alerts are enabled, but notifications are
disabled, unless channel id's are specifically set. The following Terraform
variables are used:
- `alerting_enabled` - defaults to true
- `alerting_notification_channel_ids` - the notification channel(s) to send alerts to

## Cluster Notifications

GKE clusters have the capability to send notifications to Pub/Sub topics when certain events occur,
such as available upgrades, upgrade and security bulletins. GKE can publish those events as messages
to a Pub/Sub topic. You can then subscribe to the topic and receive notifications about the events.

To receieve these events, a Pub/Sub topic and a subscription to that topic will need to be created and
the topic id will need to be passed to the `cluster_notifications_topic_id` variable.

See [Receive cluster notifications](https://cloud.google.com/kubernetes-engine/docs/how-to/cluster-notifications)

## Usage Examples

The [examples](examples/) directory contains examples of use. A basic usage
example is available in [examples/root-example](examples/root-example/).

## Upgrades

Occasionally you may find that this module wants to delete a node pool and
re-create it. This is usually due to some fundamental change to the pool
configuration such as a change of OS or a security-hardening change.

To allow this with minimal downtime, following the following process:

1. In the Google Cloud Console manually provision a temporary node pool matching
   the current pool.
2. Use [kubectl
   drain](https://kubernetes.io/docs/tasks/administer-cluster/safely-drain-node/)
   to drain and cordon existing nodes in the original node pool one-by-one.
3. Apply the configuration to replace the original node pool.
4. Drain each of the temporary node pool nodes.
5. Delete the temporary node pool.
