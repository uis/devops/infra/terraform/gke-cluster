# kics-scan disable=87065ef8-de9b-40d8-9753-f4a4303e27a4
# Configuration above is to suppress following entities in kics vulnerability report
# - "Privileged containers lack essential security restrictions and should be avoided by removing the
# 'privileged' flag or by changing its value to false"

# Configurations for enabling IP masquerade agent.
# This injects the cluster with ip-masq-agent configMap parameters which allows to specify non-masquerade
# destination ranges. GKE by default has a lot pre-defined IP ranges that are non-masquerade destination ranges.
# If a pod sends a packet to a destination IP in a specified masquerade range, the node's IP is used as the packet's
# source address instead of the pod's IP. This is useful when working on a hybrid GCP and on-premise architecture.
#
# See https://cloud.google.com/kubernetes-engine/docs/how-to/ip-masquerade-agent for more information.
#
resource "kubernetes_config_map" "ip-masq-agent-config" {
  count = var.enable_ip_masq ? 1 : 0

  depends_on = [
    google_container_cluster.cluster,
    google_container_node_pool.cluster-pool-1,
  ]

  metadata {
    name      = "ip-masq-agent"
    namespace = "kube-system"

    labels = {
      maintained_by = "terraform"
    }
  }

  data = {
    config = yamlencode({
      nonMasqueradeCIDRs = var.non_masquerade_cidrs
      resyncInterval     = var.ip_masq_resync_interval
      masqLinkLocal      = var.ip_masq_link_local
    })
  }
}

resource "kubernetes_daemonset" "ip-masq-agent" {
  count = var.enable_ip_masq ? 1 : 0

  depends_on = [
    google_container_cluster.cluster,
    google_container_node_pool.cluster-pool-1,
  ]

  metadata {
    name      = "ip-masq-agent"
    namespace = "kube-system"
  }
  spec {
    selector {
      match_labels = {
        k8s-app = "ip-masq-agent"
      }
    }
    template {
      metadata {
        labels = {
          k8s-app = "ip-masq-agent"
        }
      }
      spec {
        host_network = true
        container {
          name  = "ip-masq-agent"
          image = "gcr.io/google-containers/ip-masq-agent-amd64:v2.5.0"
          args  = ["--masq-chain=IP-MASQ"]
          security_context {
            privileged = true
          }
          volume_mount {
            name       = "config"
            mount_path = "/etc/config"
          }
        }
        volume {
          name = "config"
          config_map {
            # Note this ConfigMap must be created in the same namespace as the
            # daemon pods - this spec uses kube-system
            name = "ip-masq-agent"
            # This `optional` should be added back once the terraform provider supports it.
            #optional = true
            items {
              # The daemon looks for its config in a YAML file at /etc/config/ip-masq-agent
              key  = "config"
              path = "ip-masq-agent"
            }
          }
        }
        toleration {
          effect   = "NoSchedule"
          operator = "Exists"
        }
        toleration {
          effect   = "NoExecute"
          operator = "Exists"
        }
        toleration {
          key      = "CriticalAddonsOnly"
          operator = "Exists"
        }
      }
    }
  }
}