# versions.tf specifies minimum versions for providers and terraform.

terraform {
  required_version = ">= 0.13"

  # Specify the required providers, their version restrictions and where to get
  # them.
  required_providers {
    google = {
      source                = "hashicorp/google"
      version               = ">= 4.0"
      configuration_aliases = [google.monitoring]
    }
    google-beta = {
      source  = "hashicorp/google-beta"
      version = ">= 4.0"
    }
    helm = {
      source  = "hashicorp/helm"
      version = ">= 2.5.0"
    }
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = ">= 2.10.0"
    }
  }
}
