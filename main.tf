# kics-scan disable=6ccb85d7-0420-4907-9380-50313f80946b,9192e0f9-eca5-4056-9282-ae2a736a4088
# Configuration above is to suppress following entities in kics vulnerability report
# - "Kubernetes Clusters must be created with Private Clusters enabled, meaning the 'private_cluster_config'
# must be defined and the attributes 'enable_private_nodes' and 'enable_private_endpoint' must be true"
# - "Kubernetes Clusters must have Pod Security Policy controller enabled, which means there must be a
# 'pod_security_policy_config' with the 'enabled' attribute equal to true"

# main.tf contains the top-level resources created by this module.

# The cluster itself. We use the regional versions query above to request that
# the master be upgraded to the default version available in the region for the
# selected release channel.
resource "google_container_cluster" "cluster" {
  project            = var.project
  location           = var.location
  name               = var.name
  min_master_version = var.min_master_version
  enable_autopilot   = var.enable_autopilot ? true : null
  networking_mode    = var.networking_mode
  resource_labels    = var.resource_labels

  dynamic "cluster_autoscaling" {
    for_each = var.enable_autopilot && var.service_account != "" ? ["enabled"] : []
    content {
      auto_provisioning_defaults {
        service_account = var.service_account
      }
    }
  }

  dynamic "cluster_autoscaling" {
    for_each = !var.enable_autopilot && var.autoscaling != null ? ["enabled"] : []
    content {
      autoscaling_profile = var.autoscaling_profile
    }
  }

  # Only one of ..._secondary_range or ..._ipv4_cidr_block can be set. The
  # official module
  # (https://github.com/terraform-google-modules/terraform-google-kubernetes-engine)
  # does not even support setting the ..._ipv4_cidr settings. In order to
  # preserve backward compatibility, use dynamic blocks to construct differing
  # ip_allocation_policy blocks depending on which are set.
  #
  # We can't simply set _both_ since the Google provider immediately complains
  # that the settings are incompatible. Having the variables default to "null"
  # is also incorrect since the values _must_ be strings.

  dynamic "ip_allocation_policy" {
    for_each = ((var.cluster_secondary_range_name != "") || (var.services_secondary_range_name != "")) ? ["enabled"] : []
    content {
      cluster_secondary_range_name  = var.cluster_secondary_range_name
      services_secondary_range_name = var.services_secondary_range_name
    }
  }

  dynamic "ip_allocation_policy" {
    for_each = ((var.cluster_ipv4_cidr_block != "") || (var.services_ipv4_cidr_block != "")) ? ["enabled"] : []
    content {
      cluster_ipv4_cidr_block  = var.cluster_ipv4_cidr_block
      services_ipv4_cidr_block = var.services_ipv4_cidr_block
    }
  }

  # This is required due to a long-standing issue with the google provider which can be found at https://github.com/hashicorp/terraform-provider-google/issues/10782.
  # To deploy an Autopilot cluster using google provider >4.3.0 we need to set cluster_ipv4_cidr_block & services_ipv4_cidr_block to an empty string unless
  # they've been set as input variables.
  dynamic "ip_allocation_policy" {
    for_each = (
      (var.enable_autopilot) && (var.cluster_ipv4_cidr_block == "") && (var.services_ipv4_cidr_block == "") && (var.cluster_secondary_range_name == "") && (var.services_secondary_range_name == "")
    ) ? ["enabled"] : []

    content {
      cluster_ipv4_cidr_block  = ""
      services_ipv4_cidr_block = ""
    }
  }

  # Autopilot clusters require these to be set, otherwise they default to null, which will force a cluster recreate.
  dynamic "dns_config" {
    for_each = (var.enable_autopilot && var.autopilot_cloud_dns_enabled) ? ["enabled"] : []
    content {
      cluster_dns        = "CLOUD_DNS"
      cluster_dns_domain = "cluster.local"
      cluster_dns_scope  = "CLUSTER_SCOPE"
    }
  }

  # Autopilot clusters default the gateway api channel to standard.
  dynamic "gateway_api_config" {
    for_each = (var.enable_autopilot) ? ["enabled"] : []
    content {
      channel = "CHANNEL_STANDARD"
    }
  }

  # Secret manager add-on configuration
  dynamic "secret_manager_config" {
    for_each = var.enable_secret_manager ? ["enabled"] : []
    content {
      enabled = var.enable_secret_manager
    }
  }

  # According to
  # https://cloud.google.com/monitoring/kubernetes-engine/installing, these are
  # the magic values required to enable Stackdriver monitoring.
  logging_service = "logging.googleapis.com/kubernetes"

  monitoring_service = "monitoring.googleapis.com/kubernetes"

  # We use a pool configured elsewhere so remove the default one. The
  # initial_node_count is therefore irrelevant but must be set to a non-zero
  # value to stop the Google API complaining.
  remove_default_node_pool = var.enable_autopilot ? null : true

  initial_node_count = 1

  timeouts {
    create = "30m"
    update = "30m"
    delete = "30m"
  }

  maintenance_policy {
    recurring_window {
      start_time = var.maintenance_policy.recurring_window.start_time
      end_time   = var.maintenance_policy.recurring_window.end_time
      recurrence = var.maintenance_policy.recurring_window.recurrence
    }
  }

  release_channel {
    channel = var.release_channel
  }

  addons_config {
    istio_config {
      disabled = var.istio_disabled
      auth     = var.istio_auth
    }
    dynamic "network_policy_config" {
      for_each = toset(!var.enable_autopilot ? ["enabled"] : [])
      content {
        disabled = !var.enable_network_policy ? true : false
      }
    }
  }

  dynamic "network_policy" {
    for_each = var.enable_network_policy && !var.enable_autopilot ? ["enabled"] : []
    content {
      enabled = var.enable_network_policy
    }
  }


  # Enables workload identity on the cluster.
  dynamic "workload_identity_config" {
    for_each = toset((var.enable_workload_identity && !var.enable_autopilot) ? ["placeholder"] : [])
    content {
      workload_pool = "${var.project}.svc.id.goog"
    }
  }

  # This is enabled by default by Autopilot we add it here so that we don't
  # get Terrafrom wanting to change this from true to null all the time if
  # Autopilot is enabled.
  dynamic "vertical_pod_autoscaling" {
    for_each = toset(var.enable_autopilot ? ["placeholder"] : [])
    content {
      enabled = true
    }
  }

  dynamic "notification_config" {
    for_each = toset(var.cluster_notification_enabled ? ["enabled"] : [])
    content {
      pubsub {
        enabled = true
        topic   = var.cluster_notification_topic_id
      }
    }
  }

  deletion_protection = var.deletion_protection

  # TODO: move to non beta provider
  # For istio_config
  provider = google-beta
}

# The node pool associated with the cluster. We do not specify a node version
# here because we enable auto-upgrade of the nodes so they will always be at the
# latest version.
resource "google_container_node_pool" "cluster-pool-1" {
  count = var.enable_autopilot ? 0 : 1

  project  = var.project
  location = var.location

  name    = "pool-1"
  cluster = google_container_cluster.cluster.name

  # Not used if autoscaling specified.
  # Note that this is multiplied by the number of zones in the region.
  node_count = var.autoscaling != null ? null : var.node_count

  # If we are using autoscaling we need at least one node for the
  # autoscalar to run from at startup
  initial_node_count = var.autoscaling != null ? 1 : null

  management {
    auto_repair  = true
    auto_upgrade = true
  }

  node_config {
    # Scaling a node pool machine type requires that a new node pool be created.
    # As it is not readily changed by a user of this module, it isn't really
    # "variable" so we instead call it a default.
    machine_type = var.machine_type

    # Size of the disk attached to each node, specified in GB.
    disk_size_gb = var.disk_size_gb

    # Type of the disk attached to each node (e.g. 'pd-standard' or 'pd-ssd').
    disk_type = var.disk_type

    service_account = var.service_account != "" ? var.service_account : null

    oauth_scopes = [
      "https://www.googleapis.com/auth/compute",
      "https://www.googleapis.com/auth/devstorage.read_only",
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring",
    ]

    # Tags are used to identify valid sources or targets for network firewalls.
    tags = var.cluster_tags

    shielded_instance_config {
      enable_secure_boot          = var.enable_secure_boot
      enable_integrity_monitoring = var.enable_integrity_monitoring
    }

    # Enables workload identity on the node.
    dynamic "workload_metadata_config" {
      for_each = toset(var.enable_workload_identity ? ["placeholder"] : [])
      content {
        mode = "GKE_METADATA"
      }
    }

    dynamic "sandbox_config" {
      for_each = toset(var.enable_gvisor_sandbox ? ["placeholder"] : [])
      content {
        sandbox_type = "gvisor"
      }
    }
  }

  dynamic "autoscaling" {
    for_each = var.autoscaling != null ? [var.autoscaling] : []
    content {
      min_node_count = autoscaling.value.min_node_count
      max_node_count = autoscaling.value.max_node_count
    }
  }

  # For sandbox_config
  provider = google-beta
}

# The google_client_config data source fetches a token from the Google Authorization
# server, which expires in 1 hour by default.
data "google_client_config" "default" {
}

data "google_compute_default_service_account" "default" {
}

# Give the cluster service account permission to publish to the notification topic
resource "google_pubsub_topic_iam_member" "cluster_notification_topic" {
  for_each = toset(var.cluster_notification_enabled ? ["enabled"] : [])

  topic  = var.cluster_notification_topic_id
  role   = "roles/pubsub.publisher"
  member = "serviceAccount:${local.cluster_service_account}"
}

# kubeconfig-style configuration
locals {
  kubeconfig_template = templatefile("${path.module}/kubeconfig.template.yaml", {
    name           = google_container_cluster.cluster.name
    ca_certificate = google_container_cluster.cluster.master_auth[0].cluster_ca_certificate
    endpoint       = google_container_cluster.cluster.endpoint
  })

  cluster_service_account = var.service_account != null && var.enable_autopilot == false ? var.service_account : data.google_compute_default_service_account.default.email
}
