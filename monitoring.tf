resource "google_logging_metric" "node_no_scale_down_event" {
  count = var.alerting_enabled ? 1 : 0

  name   = "gke-cluster/${var.name}-node-no-scale-down-event"
  filter = <<-EOI
    resource.type="k8s_cluster"
    resource.labels.project_id="${var.project}"
    resource.labels.location="${var.location}"
    resource.labels.cluster_name="${var.name}"
    jsonPayload.noDecisionStatus.noScaleDown.nodesTotalCount > 0
  EOI

  metric_descriptor {
    metric_kind = "DELTA"
    value_type  = "INT64"
  }
}

resource "google_logging_metric" "gke_auto_upgrade_event" {
  count = var.alerting_enabled ? 1 : 0

  name   = "gke-cluster/${var.name}-gke-auto-upgrade-event"
  filter = <<-EOI
    resource.type=("gke_cluster" OR "gke_nodepool")
    resource.labels.project_id="${var.project}"
    resource.labels.location="${var.location}"
    resource.labels.cluster_name="${var.name}"
    protoPayload.methodName="google.container.internal.ClusterManagerInternal.UpdateClusterInternal"
  EOI

  metric_descriptor {
    metric_kind = "DELTA"
    value_type  = "INT64"
  }
}

# Alert if there are more than 1 noScaleDown events in 15 mins
resource "google_monitoring_alert_policy" "too_many_node_no_scale_down_events" {
  count = var.alerting_enabled ? 1 : 0

  display_name          = "GKE cluster node no scale down (${terraform.workspace})"
  combiner              = "OR"
  enabled               = true
  notification_channels = var.alerting_notification_channel_ids

  documentation {
    mime_type = "text/markdown"
    content   = <<-EOI
      This alert checks for the GKE cluster nodes being blocked 
      from being deleted by cluster autoscaler.
    EOI
  }

  conditions {
    display_name = "More than 1 noScaleDown event detected within the last 15 minutes"

    condition_threshold {
      filter          = <<-EOI
        resource.label.project_id="${var.project}"
        resource.type="k8s_cluster"
        AND metric.type="logging.googleapis.com/user/${google_logging_metric.node_no_scale_down_event[0].id}"
      EOI
      duration        = "900s"
      comparison      = "COMPARISON_GT"
      threshold_value = "1"

      aggregations {
        per_series_aligner   = "ALIGN_SUM"
        cross_series_reducer = "REDUCE_SUM"
        alignment_period     = "900s"
      }
    }
  }

  provider = google.monitoring
}

# Alert if GKE updates the cluster or nodepool automatically
resource "google_monitoring_alert_policy" "gke_auto_upgrade" {
  count = var.alerting_enabled ? 1 : 0

  display_name          = "GKE cluster auto-upgrade (${terraform.workspace})"
  combiner              = "OR"
  enabled               = true
  notification_channels = var.alerting_notification_channel_ids

  documentation {
    mime_type = "text/markdown"
    content   = <<-EOI
      This alert checks for the GKE cluster being auto-upgraded.
      Please check cluster is still operating normally.
    EOI
  }

  conditions {
    display_name = "UpdateClusterInternal log event detected"
    condition_threshold {
      filter          = <<-EOI
        resource.label.project_id="${var.project}"
        AND resource.type="global"
        AND metric.type="logging.googleapis.com/user/${google_logging_metric.gke_auto_upgrade_event[0].id}"
      EOI
      duration        = "120s"
      comparison      = "COMPARISON_GT"
      threshold_value = 0
    }
  }

  provider = google.monitoring
}
